import { Component, OnInit } from '@angular/core';
import { DecorationService } from '../../../service/decoration/decoration.service';
import { Decoration } from '../../../model/Decoration';
import { Package } from '../../../model/Package';

@Component({
  selector: 'app-decoration-list',
  templateUrl: './decoration-list.component.html',
  styleUrls: ['./decoration-list.component.css']
})
export class DecorationListComponent implements OnInit {

  private headers: string[] = ["DECORATION", "PACKAGE TYPE", "DESCRIPTION", "PRICE", "ACTION"];
  private decorationsList: Array<Decoration>;
  private decoration: Decoration;
  private package: Package;
  private isOpen: boolean = false;
  private isEdit: boolean = false;

  constructor(private actionService: DecorationService) { }

  ngOnInit() {
    this.getDecorationList();
  }

  private getDecorationList() {
    this.actionService.getAllDecorationList().subscribe((data) => {
      this.decorationsList = data;
    })
  }

  private editDecoration(selDecoration: Decoration, addNew ?: boolean, selPackage?: Package) {
    this.isOpen = !this.isOpen;
    this.isEdit = true;
    this.decoration = selDecoration;
    if (addNew) {
      this.isEdit = false;
      const newPackage : Package = new Package();
      this.package = newPackage;
    } else {
      this.package = selPackage;
    }
  }

}
