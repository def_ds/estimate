import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecorationListComponent } from './decoration-list.component';

describe('DecorationListComponentComponent', () => {
  let component: DecorationListComponent;
  let fixture: ComponentFixture<DecorationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecorationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecorationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
