import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Decoration } from '../../../../model/Decoration';
import { PackageType } from '../../../../model/enum/PackageType';
import { Package } from '../../../../model/Package';
import { NgForm } from '@angular/forms';
import { DecorationService } from '../../../../service/decoration/decoration.service';

@Component({
  selector: 'app-decoration-edit',
  templateUrl: './decoration-edit.component.html',
  styleUrls: ['./decoration-edit.component.css']
})
export class DecorationEditComponent implements OnInit, OnChanges {
  @Input() public isOpen: boolean = false;
  @Input() public isEdit: boolean = false;
  @Input() private decoration: Decoration;
  @Input() private package: Package;
  @ViewChild('content', {static: false}) content: ElementRef;
  private packageTypes: string[];

  constructor(private modalService: NgbModal, private actionService: DecorationService) {
    this.packageTypes = Object.keys(PackageType);
  }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.open(this.content);
  }

  public open(content) {
    if (this.isOpen) {
      this.modalService.open(content);
    } else {
      this.modalService.dismissAll();
    }
  }

  addPackage(form: NgForm) {
    let newPackage: Package = form.value;
    if (this.checkDuplicationPackage(newPackage)) {
      alert("Duplication");
      return;
    }

    this.decoration.packageList.push(newPackage);
    this.actionService.addPackageToDecoration(this.decoration).subscribe((data) => {
      this.modalService.dismissAll();
    })
  }

  checkDuplicationPackage(newPackage: Package) {
    let packageType = this.decoration.packageList
      .find((s) => s.packageType === newPackage.packageType);
    return packageType != null;
  }

}
