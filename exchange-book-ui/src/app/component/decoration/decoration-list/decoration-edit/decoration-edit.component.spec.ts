import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecorationEditComponent } from './decoration-edit.component';

describe('DecorationEditComponent', () => {
  let component: DecorationEditComponent;
  let fixture: ComponentFixture<DecorationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecorationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecorationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
