import { TestBed } from '@angular/core/testing';

import { DecorationService } from './decoration.service';

describe('DecorationServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DecorationService = TestBed.get(DecorationService);
    expect(service).toBeTruthy();
  });
});
