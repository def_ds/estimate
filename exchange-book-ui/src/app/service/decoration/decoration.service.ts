import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Decoration } from '../../model/Decoration';

@Injectable({
  providedIn: 'root'
})
export class DecorationService implements OnInit{
  constructor(private http: HttpClient) {}

  ngOnInit(): void { }

  getAllDecorationList() {
    return this.http.get<Array<Decoration>>("http://localhost:8080/decoration/");
  }

  getDecorationById(id: string) {
    return this.http.get<Array<Decoration>>("http://localhost:8080/decoration/" + id);
  }

  addPackageToDecoration(decoration: Decoration) {
    return this.http.post("http://localhost:8080/decoration/", decoration);
  }

}
