import { Base } from './Base';
import { Package } from './Package';

export class Decoration extends Base {
  private _name : string;
  private _description : string;
  private _packageList : Package[];

  get name(): string {
    return this._name;
  }

  get description(): string {
    return this._description;
  }

  get packageList(): Package[] {
    return this._packageList;
  }

  set name(value: string) {
    this._name = value;
  }

  set description(value: string) {
    this._description = value;
  }

  set packageList(value: Package[]) {
    this._packageList = value;
  }
}
