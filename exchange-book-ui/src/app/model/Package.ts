import { Base } from './Base';
import { PackageType } from './enum/PackageType';

export class Package extends Base {
  private _price: number;
  private _description: string;
  private _packageType: PackageType;

  get price(): number {
    return this._price;
  }

  set price(value: number) {
    this._price = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get packageType(): PackageType {
    return this._packageType;
  }

  set packageType(value: PackageType) {
    this._packageType = value;
  }
}
