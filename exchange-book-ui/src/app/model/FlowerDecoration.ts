import { Base } from './Base';
import { Decoration } from './Decoration';
import { User } from './User';

export class FlowerDecoration extends Base {
  private _name : string;
  private _packageList : Decoration[];
  private _flowerDecorationList : FlowerDecoration[];
  private _user: User;


  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get packageList(): Decoration[] {
    return this._packageList;
  }

  set packageList(value: Decoration[]) {
    this._packageList = value;
  }

  get flowerDecorationList(): FlowerDecoration[] {
    return this._flowerDecorationList;
  }

  set flowerDecorationList(value: FlowerDecoration[]) {
    this._flowerDecorationList = value;
  }

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }
}
