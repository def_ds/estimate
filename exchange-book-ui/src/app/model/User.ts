import { Base } from './Base';

export class User extends Base {
  private username : string;
  private email : string;
  private phone : string;
}
