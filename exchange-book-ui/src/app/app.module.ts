import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DecorationListComponent } from './component/decoration/decoration-list/decoration-list.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DecorationEditComponent } from './component/decoration/decoration-list/decoration-edit/decoration-edit.component';
import { FormsModule } from '@angular/forms';
import { DecorationAddComponent } from './component/decoration/decoration-list/decoration-add/decoration-add.component';

@NgModule({
  declarations: [
    AppComponent,
    DecorationListComponent,
    DecorationEditComponent,
    DecorationAddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
