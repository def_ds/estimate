package com.exchange.book.controller.interfaces;

import com.exchange.book.model.BaseEntity;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AbstractGenericControllerInterface<T extends BaseEntity> {

    ResponseEntity<List<T>> getList();
    ResponseEntity<T> getEntityById(String id);
    ResponseEntity<T> addEntity(T entity);

}
