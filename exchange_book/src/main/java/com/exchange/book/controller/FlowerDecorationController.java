package com.exchange.book.controller;

import com.exchange.book.model.FlowerDecorationEntity;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/flower")
public class FlowerDecorationController extends AbstractGenericController<FlowerDecorationEntity> {

}
