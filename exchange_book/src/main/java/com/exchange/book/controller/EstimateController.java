package com.exchange.book.controller;

import com.exchange.book.model.EstimateEntity;
import com.exchange.book.service.interfaces.EstimateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/estimate")
public class EstimateController {

    private EstimateService estimateService;

    @Autowired
    public EstimateController(EstimateService estimateService) {
        this.estimateService = estimateService;
    }

    @GetMapping(value = "/")
    public ResponseEntity<List<EstimateEntity>> getAllEstimates() {
        List<EstimateEntity> estimateEntities = estimateService.getAll();
        return new ResponseEntity<>(estimateEntities, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<EstimateEntity> getEstimateById(@PathVariable String id) {
        EstimateEntity estimateEntity = estimateService.getById(id);
        return new ResponseEntity<>(estimateEntity, HttpStatus.OK);
    }

}
