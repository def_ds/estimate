package com.exchange.book.controller;

import com.exchange.book.controller.interfaces.AbstractGenericControllerInterface;
import com.exchange.book.model.BaseEntity;
import com.exchange.book.service.interfaces.GenericService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class AbstractGenericController<T extends BaseEntity>
    implements AbstractGenericControllerInterface<T> {

    @Autowired
    protected GenericService<T> service;

    @CrossOrigin("http://localhost:4200")
    @GetMapping("/")
    public ResponseEntity<List<T>> getList() {
        List<T> entityList = service.getList();
        return new ResponseEntity<>(entityList, HttpStatus.OK);
    }

    @CrossOrigin("http://localhost:4200")
    @GetMapping(value = "/{id}")
    public ResponseEntity<T> getEntityById(@PathVariable String id) {
        T entity = service.getById(id);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @CrossOrigin("http://localhost:4200")
    @PostMapping(value = "/")
    public ResponseEntity<T> addEntity(@RequestBody T entity) {
        T savedEntity = service.addItem(entity);
        return new ResponseEntity<>(savedEntity, HttpStatus.OK);
    }

}
