package com.exchange.book.controller;

import com.exchange.book.model.DecorationEntity;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/decoration")
public class DecorationController extends AbstractGenericController<DecorationEntity> {

}
