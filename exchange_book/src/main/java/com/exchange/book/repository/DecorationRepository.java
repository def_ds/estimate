package com.exchange.book.repository;

import com.exchange.book.model.DecorationEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DecorationRepository extends JpaRepository<DecorationEntity, String> {

}
