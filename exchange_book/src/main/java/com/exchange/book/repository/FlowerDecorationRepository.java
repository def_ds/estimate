package com.exchange.book.repository;

import com.exchange.book.model.FlowerDecorationEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowerDecorationRepository extends JpaRepository<FlowerDecorationEntity, String> {

}
