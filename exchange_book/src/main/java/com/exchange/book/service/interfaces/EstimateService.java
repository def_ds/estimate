package com.exchange.book.service.interfaces;

import com.exchange.book.model.EstimateEntity;

import java.util.List;

public interface EstimateService {

    List<EstimateEntity> getAll();
    EstimateEntity getById(String id);
}
