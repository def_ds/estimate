package com.exchange.book.service.interfaces;

import com.exchange.book.model.FlowerDecorationEntity;

import java.util.List;

//TODO: To delete if not add some unique method
public interface FlowerDecorationService {

    List<FlowerDecorationEntity> getAll();
    FlowerDecorationEntity getById(String id);
    FlowerDecorationEntity addDecoration(FlowerDecorationEntity decorationEntity);
}
