package com.exchange.book.service;

import com.exchange.book.exception.NoItemFoundException;
import com.exchange.book.model.FlowerDecorationEntity;
import com.exchange.book.repository.FlowerDecorationRepository;
import com.exchange.book.service.interfaces.GenericService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FlowerDecorationImpl implements GenericService<FlowerDecorationEntity> {

    private FlowerDecorationRepository flowerDecorationRepository;

    @Autowired
    public FlowerDecorationImpl(FlowerDecorationRepository flowerDecorationRepository) {
        this.flowerDecorationRepository = flowerDecorationRepository;
    }

    @Override
    public List<FlowerDecorationEntity> getList() {
        return flowerDecorationRepository.findAll();
    }

    @Override
    public FlowerDecorationEntity getById(String id) {
        Optional<FlowerDecorationEntity> flowerDecorationEntity = flowerDecorationRepository.findById(id);
        if(!flowerDecorationEntity.isPresent()) {
            throw new NoItemFoundException("Estimate not found");
        }
        return flowerDecorationEntity.get();
    }

    @Override
    public FlowerDecorationEntity addItem(FlowerDecorationEntity decorationEntity) {
        FlowerDecorationEntity flowerDecoration = null;
        if (decorationEntity != null) {
            flowerDecoration = flowerDecorationRepository.saveAndFlush(decorationEntity);
        }
        return flowerDecoration;
    }
}
