package com.exchange.book.service.interfaces;

import com.exchange.book.model.BaseEntity;

import java.util.List;

public interface GenericService<T extends BaseEntity> {

    List<T> getList();
    T getById(String id);
    T addItem(T decorationEntity);

}
