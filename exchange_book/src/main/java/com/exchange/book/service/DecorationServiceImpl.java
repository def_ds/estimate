package com.exchange.book.service;

import com.exchange.book.exception.NoItemFoundException;
import com.exchange.book.model.DecorationEntity;
import com.exchange.book.repository.DecorationRepository;
import com.exchange.book.service.interfaces.GenericService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DecorationServiceImpl implements GenericService<DecorationEntity> {

    private DecorationRepository decorationRepository;

    @Autowired
    public DecorationServiceImpl(DecorationRepository decorationRepository) {
        this.decorationRepository = decorationRepository;
    }

    @Override
    public List<DecorationEntity> getList() {
        return decorationRepository.findAll();
    }

    @Override
    public DecorationEntity getById(String id) {
        Optional<DecorationEntity> decorationEntity = decorationRepository.findById(id);
        if(!decorationEntity.isPresent()) {
            throw new NoItemFoundException("Estimate not found");
        }
        return decorationEntity.get();
    }

    @Override
    public DecorationEntity addItem(DecorationEntity decorationEntity) {
        DecorationEntity decoration = null;
        if (decorationEntity != null) {
            decoration = decorationRepository.saveAndFlush(decorationEntity);
        }
        return decoration;
    }

}
