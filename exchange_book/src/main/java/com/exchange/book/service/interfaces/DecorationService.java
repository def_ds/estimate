package com.exchange.book.service.interfaces;

import com.exchange.book.model.DecorationEntity;

import java.util.List;

//TODO: To delete if not add some unique method
public interface DecorationService {

    List<DecorationEntity> getAll();
    DecorationEntity getById(String id);
    DecorationEntity addDecoration(DecorationEntity decorationEntity);
}
