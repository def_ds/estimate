package com.exchange.book.service;

import com.exchange.book.exception.NoItemFoundException;
import com.exchange.book.model.EstimateEntity;
import com.exchange.book.repository.EstimateRepository;
import com.exchange.book.service.interfaces.EstimateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EstimateServiceImpl implements EstimateService {

    private EstimateRepository estimateRepository;

    @Autowired
    public EstimateServiceImpl(EstimateRepository estimateRepository) {
        this.estimateRepository = estimateRepository;
    }

    @Override
    public List<EstimateEntity> getAll() {
        return estimateRepository.findAll();
    }

    @Override
    public EstimateEntity getById(String id) {
        Optional<EstimateEntity> estimateEntity = estimateRepository.findById(id);
        if(!estimateEntity.isPresent()) {
            throw new NoItemFoundException("Estimate not found");
        }
        return estimateEntity.get();
    }
}
