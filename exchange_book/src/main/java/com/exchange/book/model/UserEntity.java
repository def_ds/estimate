package com.exchange.book.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserEntity extends BaseEntity {

    @NotEmpty
    @Column(name = "USERNAME")
    private String username;

    @NotEmpty
    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE")
    private String phone;

}
