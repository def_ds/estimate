package com.exchange.book.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class EstimateEntity extends BaseEntity {

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="ESTIMATE_ID")
    private List<DecorationEntity> decorationList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="ESTIMATE_ID")
    private List<FlowerDecorationEntity> flowerDecorationList = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity user;


}
