package com.exchange.book.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"NAME"})})
public class FlowerDecorationEntity extends BaseEntity {

    @NotBlank
    @Column(name = "NAME")
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "FLOWER_DECORATION_ID")
    private List<PackageEntity> packageList;

}
