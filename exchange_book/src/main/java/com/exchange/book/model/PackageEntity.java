package com.exchange.book.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PackageEntity extends BaseEntity {

    @NotNull
    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "DESCRIPTION")
    private String description;

    @NotNull
    @Column(name = "PACKAGE")
    @Enumerated(EnumType.STRING)
    private PackageType packageType;

    public enum PackageType {
        BASIC, STANDARD, PREMIUM, LUXURY, DEFAULT
    }

}
