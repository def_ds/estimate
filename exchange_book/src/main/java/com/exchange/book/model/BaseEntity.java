package com.exchange.book.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @Column(name = "ID", nullable = false)
    private String id = UUID.randomUUID().toString();

}
