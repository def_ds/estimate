package com.exchange.book.init;

import com.exchange.book.model.DecorationEntity;
import com.exchange.book.model.EstimateEntity;
import com.exchange.book.model.PackageEntity;
import com.exchange.book.model.UserEntity;
import com.exchange.book.repository.EstimateRepository;
import com.exchange.book.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import javax.transaction.Transactional;

@Component
public class EntityInitializer implements CommandLineRunner {

    private UserRepository userRepository;

    private EstimateRepository estimateRepository;

    @Autowired
    public EntityInitializer(UserRepository userRepository, EstimateRepository estimateRepository) {
        this.userRepository = userRepository;
        this.estimateRepository = estimateRepository;
    }

    @Override
    @Transactional
    public void run(String... args) {
        PackageEntity packageEntityBasic =
            PackageEntity.builder().packageType(PackageEntity.PackageType.BASIC).price(new BigDecimal(10))
                .description("Test description basic").build();

        PackageEntity packageEntityPremium =
            PackageEntity.builder().packageType(PackageEntity.PackageType.PREMIUM).price(new BigDecimal(14))
                .description("Test description premium").build();

        PackageEntity packageEntityBasic2 =
            PackageEntity.builder().packageType(PackageEntity.PackageType.LUXURY).price(new BigDecimal(10))
                .description("Test description basic").build();

        PackageEntity packageEntityPremium2 =
            PackageEntity.builder().packageType(PackageEntity.PackageType.DEFAULT).price(new BigDecimal(14))
                .description("Test description basic").build();

        DecorationEntity decorationEntity = DecorationEntity.builder().name("Car Decotarion")
                .packageList(Arrays.asList(packageEntityBasic, packageEntityPremium)).build();

        DecorationEntity decorationEntity2 = DecorationEntity.builder().name("Table Decotarion")
            .packageList(Arrays.asList(packageEntityBasic2, packageEntityPremium2)).build();

        UserEntity userEntity = UserEntity.builder().username("Dawid").email("op@op.pl").phone("523-435-645").build();

        UserEntity userEntity2 = UserEntity.builder().username("Magda").email("op2@op.pl").phone("523-435-643").build();

        EstimateEntity estimateEntity = EstimateEntity.builder().user(userEntity)
            .decorationList(Collections.singletonList(decorationEntity)).build();

        EstimateEntity estimateEntity2 = EstimateEntity.builder().user(userEntity2)
            .decorationList(Collections.singletonList(decorationEntity2)).build();

        userRepository.saveAll(Arrays.asList(userEntity, userEntity2));
        estimateRepository.saveAll(Arrays.asList(estimateEntity, estimateEntity2));
    }
}
